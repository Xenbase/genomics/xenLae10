#!/usr/bin/env python3
import sys
import datetime

date_ver = datetime.datetime.now().strftime("%Y-%m-%d")

filename_list = 'XENLA_JGIv91-to-refseq101.symbols.2023_07_21.sort.tsv'

filename_out = "XENLA_gene_info.JGIv91.%s.tsv" % date_ver
filename_log = "XENLA_gene_info.JGIv91.%s.log" % date_ver

ncbi2symbol = dict()
f_list = open(filename_list, 'r')
for line in f_list:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    ncbi_gene_id = tokens[1].split('|')[-1]
    ncbi2symbol[ncbi_gene_id] = tmp_symbol
f_list.close()


# nova2.S	MGC53786|XP_041430006.1|XM_041574072.1|GeneID:379350	nova2.S|Xelaev18042375m.g	1.000
# colca2.L	LOC108696478|XP_041425218.1|XM_041569284.1|GeneID:108696478	colca2.L|Xelaev18035480m.g	0.835

f_out = open(filename_out, 'w')
f_log = open(filename_log, 'w')
f_gene_info = open("../XENLA_gene_info.active.tsv", "r")
for line in f_gene_info:
    if line.startswith("#"):
        f_out.write("# LAST_MODIFIED: %s\n" % date_ver)
        f_out.write("# GENE_SYMBOL\tNCBI_GENE_ID\tXB_GENE_ID\tGENE_NAME\n")
        f_log.write("# LAST_MODIFIED: %s\n" % date_ver)
        continue
    old_line = line.strip()
    tokens = old_line.split("\t")
    tmp_symbol = tokens[0]
    tmp_ncbi_gene_id = tokens[1]
    new_line = old_line
    if tmp_ncbi_gene_id in ncbi2symbol:
        if ncbi2symbol[tmp_ncbi_gene_id] != tmp_symbol:
            new_line = "%s\t%s" % (ncbi2symbol[tmp_ncbi_gene_id], 
                                   "\t".join(tokens[1:]))
    if new_line != old_line:
        f_out.write("%s\n" % new_line)
        f_log.write("+++\t%s\n" % new_line)
        f_log.write("---\t%s\n" % old_line)
    else:
        f_out.write("%s\n" % old_line)
f_gene_info.close()
f_log.close()
f_out.close()
