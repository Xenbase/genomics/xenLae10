#!/usr/bin/env python3
import sys
import re

filename_tsv = 'XENLA_xenLae10_refseq101.prot_names.tsv'
filename_prot_id_list = '../XENLA_prot_id_list.active.csv'

gene_info = dict()
f_tsv = open(filename_tsv, 'r')

f_log = open('XENLA_gene_info.refseq101.log', 'w')
for line in f_tsv:
    tokens = line.strip().split("\t")
    gene_id = "%s\t%s" % (tokens[1], tokens[2])
    tmp_symbol = tokens[3]
    tmp_name = tokens[4].split(' isoform ')[0].strip()
    tmp_name = re.sub(r' precursor$', '', tmp_name)
    if gene_id not in gene_info:
        gene_info[gene_id] = {'symbol': tmp_symbol, 'name': tmp_name}
    else:
        if gene_info[gene_id]['symbol'] != tmp_symbol:
            sys.stderr.write("DiffSymbol\t%s\t%s\t%s\n" %
                             (gene_id, gene_info[gene_id]['symbol'],
                              tmp_symbol))
            f_log.write("DiffSymbol\t%s\t%s\t%s\n" %
                        (gene_id, gene_info[gene_id]['symbol'], tmp_symbol))
        elif gene_info[gene_id]['name'] != tmp_name:
            f_log.write("DiffName\t%s\t%s\t%s\t%s\n" %
                        (gene_id, tmp_symbol,
                         gene_info[gene_id]['name'], tmp_name))
            if gene_info[gene_id]['name'].find('uncharacterize') >= 0 \
               and tmp_name.find('uncharacterize') < 0:
                gene_info[gene_id]['name'] = tmp_name
f_tsv.close()
f_log.close()

f_out = open('XENLA_gene_info.refseq101.tsv', 'w')
for tmp_id, tmp in sorted(gene_info.items(),
                          key=lambda item: item[1]['symbol']):
    f_out.write("%s\t%s\t%s\n" % (tmp['symbol'], tmp_id, tmp['name']))
f_out.close()
