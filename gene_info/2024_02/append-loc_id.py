#!/usr/bin/env python3
import sys

ncbi2loc = dict()
f_loc = open('../XENLA_gene_info_loc.XB-GFF.2024_02_15.tsv', 'r')
for line in f_loc:
    tokens = line.strip().split("\t")
    tmp_xb_id = tokens[1]
    tmp_ncbi_id = tokens[2]
    tmp_xb_loc = tokens[4]
    ncbi2loc[tmp_ncbi_id] = {'xb_id': tmp_xb_id, 'xb_loc': tmp_xb_loc}
f_loc.close()

filename_tsv_in = "XENLA_gene_info.XB2024_02.only_RefSeq_to_create-XB-ID.uniq_symbol.tsv"

filename_in = "XENLA_gene_info.XB2024_02.only_RefSeq_to_create-XB-ID.uniq_symbol.tsv"
filename_out = "XENLA_gene_info_loc.XB2024_02.only_RefSeq_to_create-XB-ID.uniq_symbol.tsv"
filename_revert = "XENLA_gene_info.XB2024_02.only_RefSeq.XB-ID_from_gff3.tsv"

f_in = open(filename_in, 'r')
f_revert = open(filename_revert, 'w')
f_out = open(filename_out, 'w')

count_missed = 0
for line in f_in:
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    tmp_xb_id = tokens[1]
    tmp_ncbi_id = tokens[2]
    tmp_ens_id = tokens[3]
    tmp_name = tokens[4]

    if not tmp_ncbi_id in ncbi2loc:
        count_missed += 1
        f_out.write("%s\t%s\t%s\t%s\t%s\t%s\n" % 
                    (tmp_symbol, "NA", tmp_ncbi_id, tmp_ens_id, "NA", tmp_name))
    else:
        tmp_loc_id = ncbi2loc[tmp_ncbi_id]['xb_loc']
        if ncbi2loc[tmp_ncbi_id]['xb_id'] != 'N.A.':
            tmp_xb_id = ncbi2loc[tmp_ncbi_id]['xb_id']
            f_revert.write("%s\t%s\t%s\t%s\t%s\n" % 
                           (tmp_symbol, tmp_xb_id, tmp_ncbi_id, tmp_ens_id, tmp_name))
        else:
            f_out.write("%s\t%s\t%s\t%s\t%s\t%s\n" % 
                        (tmp_symbol, "NA", tmp_ncbi_id, tmp_ens_id, tmp_loc_id, tmp_name))

f_out.close()
f_in.close()
f_revert.close()

sys.stderr.write("Missed loc_id: %d\n" % count_missed)
