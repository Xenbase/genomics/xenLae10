#!/usr/bin/env python3
import sys

# XTR_XB-GENE	XTR_XB-GENEPAGE	XTR_Symbol	XTR_NCBI	XTR_GeneModel	InParanoid_XLAL_NCBIID	InParanoid_XLAL_Symbol	XLAL_XB-GENE	XB-GENEPAGE	New_XBGENE_Symbol	InParanoid_XLAS_NCBIID	InParanoid_XLAS_Symbol	XLAS_XB-GENE	XB-GENEPAGE	New_XBGENE_Symbol
# XB-GENE-29093771	XB-GENEPAGE-29093770	LOC116408069	116408069	XBXT10g008635	121398344	LOC121398344.L	XB-GENE-29093772	XB-GENEPAGE-29093770	LOC121398344.L	121399143	LOC121399143.S	XB-GENE-29093772	XB-GENEPAGE-29093770	LOC121399143.S

gene_info_xb = []
gene_info_ncbi = []

f_gene_info = open("../XENLA_gene_info.2023-11-27.tsv", "r")
for line in f_gene_info:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    gene_info_xb.append(tokens[1])
    gene_info_ncbi.append(tokens[2])
f_gene_info.close()

ncbi2xb = dict()

f_AB_list = open('../../xenbase/XENLA_XB-GENE-IDs_from_XENTR-GenePage.2024_02_23.AB.tsv', 'r')
for line in f_AB_list:
    tokens = line.strip().split("\t")
    XL_L_ncbi_id = "GeneID:%s" % tokens[5]
    XL_L_xb_id = tokens[7]
    if XL_L_xb_id in gene_info_xb:
        sys.stderr.write("Already taken: %s\n" % XL_L_xb_id)
        continue

    XL_S_ncbi_id = "GeneID:%s" % tokens[10]
    XL_S_xb_id = tokens[12]
    if XL_S_xb_id in gene_info_xb:
        sys.stderr.write("Already taken: %s\n" % XL_S_xb_id)
        continue

    ncbi2xb[XL_S_ncbi_id] = XL_S_xb_id
f_AB_list.close()

f_out = open("XENLA_gene_info.XB2024_02.only_RefSeq_from_GenePage.tsv", "w")
f_log = open("XENLA_gene_info.XB2024_02.only_RefSeq_from_GenePage.log", "w")
f_new_multi = open("XENLA_gene_info.XB2024_02.only_RefSeq_to_create-XB-ID.multi_symbol.tsv", "w")
f_new_uniq = open("XENLA_gene_info.XB2024_02.only_RefSeq_to_create-XB-ID.uniq_symbol.tsv", "w")

symbol_count = dict()
f_list = open("XENLA_gene_info.XB2024_02.only_RefSeq.tsv", "r")
for line in f_list:
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    if tmp_symbol not in symbol_count:
        symbol_count[tmp_symbol] = 0
    symbol_count[tmp_symbol] += 1
f_list.close()

f_list = open("XENLA_gene_info.XB2024_02.only_RefSeq.tsv", "r")
for line in f_list:
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    tmp_ncbi_id = tokens[2]
    if tmp_ncbi_id in ncbi2xb:
        tmp_xb_id = ncbi2xb[tmp_ncbi_id]
        f_out.write("%s\t%s\t%s\t%s\t%s\n" % (tokens[0], tmp_xb_id, tmp_ncbi_id, tokens[3], tokens[4]))
        f_log.write("ABList\t%s\t%s\t%s\t%s\t%s\n" % (tokens[0], tmp_xb_id, tmp_ncbi_id, tokens[3], tokens[4]))
        f_log.write("RefSeq\t%s\n\n" % line.strip())
    else:
        if symbol_count[tmp_symbol] == 1:
            f_new_uniq.write("%s\n" % line.strip())
        else:
            f_new_multi.write("%s\n" % line.strip())
f_list.close()

f_out.close()
f_log.close()
f_new_multi.close()
f_new_uniq.close()
