#!/usr/bin/env python3

xb_xb_list = dict()
xb_ncbi_list = dict()

filename_xb = "../XENLA_gene_info.Xenbase.2024-02-01.tsv"
#filename_active = "../XENLA_gene_info.refseq101_coding.tsv"
filename_active = "../XENLA_gene_info.2023-11-27.tsv"
filename_nc = "../XENLA_gene_info.refseq101_noncoding.tsv"

nc_ncbi_list = dict()
f_nc = open(filename_nc, 'r')
for line in f_nc:
    tokens = line.strip().split("\t")
    nc_ncbi_list[tokens[2]] = line.strip()
f_nc.close()

f_xb = open(filename_xb, 'r')
for line in f_xb:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    tmp_xb_id = tokens[1]
    tmp_ncbi_id = tokens[2]
    xb_xb_list[tmp_xb_id] = {'ncbi': tmp_ncbi_id,
                             'symbol': tmp_symbol, 'line': line.strip()}
    xb_ncbi_list[tmp_ncbi_id] = {'xb': tmp_xb_id,
                                 'symbol': tmp_symbol, 'line': line.strip()}
f_xb.close()

active_xb_list = dict()
active_ncbi_list = dict()

f_active = open(filename_active, 'r')
for line in f_active:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_symbol = tokens[0]
    tmp_xb_id = tokens[1]
    tmp_ncbi_id = tokens[2]
    active_xb_list[tmp_xb_id] = {'ncbi': tmp_ncbi_id,
                                 'symbol': tmp_symbol,
                                 'line': line.strip()}
    active_ncbi_list[tmp_ncbi_id] = {'xb': tmp_xb_id,
                                     'symbol': tmp_symbol,
                                     'line': line.strip()}
f_active.close()

print("XB_list", len(xb_xb_list), len(xb_ncbi_list))
print("Active list", len(active_xb_list), len(active_ncbi_list))

overlap_ncbi_set = set(xb_ncbi_list.keys()).intersection(
                       set(active_ncbi_list.keys()))
print("Overlap NCBI: ", len(overlap_ncbi_set))

f_match_good = open('XENLA_gene_info.XB2024_02.id-match-symbol-match.tsv', 'w')
f_match_change = open('XENLA_gene_info.XB2024_02.id-match-symbol-change.tsv', 'w')
f_match_change_log = open('XENLA_gene_info.XB2024_02.id-match-symbol-change.log', 'w')
f_new_xb_id = open('XENLA_gene_info.XB2024_02.new-XBID.tsv', 'w')
f_new_xb_id_log = open('XENLA_gene_info.XB2024_02.new-XBID.log', 'w')
f_diff_xb_id = open('XENLA_gene_info.XB2024_02.diff-XBID.tsv', 'w')
f_diff_xb_id_log = open('XENLA_gene_info.XB2024_02.diff-XBID.log', 'w')

for tmp_ncbi_id in sorted(list(overlap_ncbi_set)):
    if active_ncbi_list[tmp_ncbi_id]['xb'] == xb_ncbi_list[tmp_ncbi_id]['xb']:
        if active_ncbi_list[tmp_ncbi_id]['symbol'] == xb_ncbi_list[tmp_ncbi_id]['symbol']:
            f_match_good.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
        else:
            f_match_change.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
            f_match_change_log.write("Xenbase\t%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
            f_match_change_log.write("Refseq\t%s\n\n" % active_ncbi_list[tmp_ncbi_id]['line'])

    elif active_ncbi_list[tmp_ncbi_id]['xb'] == 'NA':
        if active_ncbi_list[tmp_ncbi_id]['symbol'] == xb_ncbi_list[tmp_ncbi_id]['symbol']:
            f_new_xb_id.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
        else:
            f_new_xb_id.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
            f_new_xb_id_log.write("Xenbase\t%s\n" %
                                  xb_ncbi_list[tmp_ncbi_id]['line'])
            f_new_xb_id_log.write("Refseq\t%s\n\n" %
                                  active_ncbi_list[tmp_ncbi_id]['line'])
    else:
        if active_ncbi_list[tmp_ncbi_id]['symbol'] == xb_ncbi_list[tmp_ncbi_id]['symbol']:
            f_diff_xb_id.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
        else:
            f_diff_xb_id.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
            f_diff_xb_id_log.write("Xenbase\t%s\n" %
                                   xb_ncbi_list[tmp_ncbi_id]['line'])
            f_diff_xb_id_log.write("Refseq\t%s\n\n" %
                                   active_ncbi_list[tmp_ncbi_id]['line'])
f_match_change.close()
f_match_good.close()

ncbi_only_set = set(active_ncbi_list.keys()) - overlap_ncbi_set
print("Only NCBI: ", len(ncbi_only_set))

f_only_ncbi = open('XENLA_gene_info.XB2024_02.only_RefSeq.tsv', 'w')
for tmp_ncbi_id in sorted(list(ncbi_only_set)):
    f_only_ncbi.write("%s\n" % active_ncbi_list[tmp_ncbi_id]['line'])
f_only_ncbi.close()

xb_only_set = set(xb_ncbi_list.keys()) - overlap_ncbi_set
print("Only Xenbase: ", len(xb_only_set))

f_only_xb = open('XENLA_gene_info.XB2024_02.only_Xenbase.tsv', 'w')
f_only_xb_nc = open('XENLA_gene_info.XB2024_02.only_Xenbase_noncoding.tsv', 'w')
for tmp_ncbi_id in sorted(list(xb_only_set)):
    if tmp_ncbi_id in nc_ncbi_list:
        f_only_xb_nc.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
    else:
        f_only_xb.write("%s\n" % xb_ncbi_list[tmp_ncbi_id]['line'])
f_only_xb.close()
f_only_xb_nc.close()
