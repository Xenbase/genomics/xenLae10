#!/bin/bash

## EnsEMBL
curl --output-dir ./raw -O https://ftp.ensembl.org/pub/rapid-release/species/Xenopus_laevis/GCA_017654675.1/ensembl/geneset/2022_01/Xenopus_laevis-GCA_017654675.1-2022_01-genes.gff3.gz
curl --output-dir ./raw -O https://ftp.ensembl.org/pub/rapid-release/species/Xenopus_laevis/GCA_017654675.1/ensembl/geneset/2022_01/Xenopus_laevis-GCA_017654675.1-2022_01-genes.gtf.gz

## RefSeq
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_other/Xenopus_laevis/latest_assembly_versions/GCF_017654675.1_Xenopus_laevis_v10.1/GCF_017654675.1_Xenopus_laevis_v10.1_genomic.gff.gz
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_other/Xenopus_laevis/latest_assembly_versions/GCF_017654675.1_Xenopus_laevis_v10.1/GCF_017654675.1_Xenopus_laevis_v10.1_assembly_report.txt
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_other/Xenopus_laevis/latest_assembly_versions/GCF_017654675.1_Xenopus_laevis_v10.1/GCF_017654675.1_Xenopus_laevis_v10.1_genomic.gtf.gz

## JGI

## Xenbase
curl --output-dir ./raw -O https://download.xenbase.org/xenbase/Genomics/JGI/Xenla10.1/XENLA_10.1_Xenbase.gff3.gz
