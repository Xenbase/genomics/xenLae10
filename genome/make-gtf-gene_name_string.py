#!/usr/bin/env python3
import sys

filename_gtf = sys.argv[1]
nearby_genes = 5

gene_list = dict()
f_gtf = open(filename_gtf, 'r')
for line in f_gtf:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    chr_id = tokens[0]
    tmp_type = tokens[2]
    if tmp_type != 'gene':
        continue
    tmp_start = int(tokens[3])
    tmp_tag = tokens[8]
    if chr_id not in gene_list:
        gene_list[chr_id] = dict()
    gene_list[chr_id][tmp_tag] = tmp_start
f_gtf.close()

print("#ChrID\tLeftGenes\tQueryGene\tRightGenes")
for tmp_chr_id in sorted(gene_list.keys()):
    sorted_gene_list = sorted(gene_list[tmp_chr_id].keys(),
                              key=gene_list[tmp_chr_id].get)

    for i in range(0, len(sorted_gene_list)):
        # ID=GeneID:116408214;Name=LOC116408214;biotype=protein_coding
        tmp_q_tokens = sorted_gene_list[i].split(';')
        tmp_q_gene = '%s|%s' % (tmp_q_tokens[1].replace('gene_name ', '').replace('"', ''), 
                                tmp_q_tokens[0].replace('gene_id ', '').replace('"', ''))

        tmp_left_idx = max([i-nearby_genes, 0])
        tmp_right_idx = min([len(sorted_gene_list), i+1+nearby_genes])
        tmp_left_gene_names = [tmp.split(';')[1].replace('gene_name ', '').replace('"', '')
                               for tmp in sorted_gene_list[tmp_left_idx:i]]
        tmp_right_gene_names = [tmp.split(';')[1].replace('gene_name', '').replace('"', '')
                                for tmp in sorted_gene_list[i+1:tmp_right_idx]]
        print("%s\t%s\t%s\t%s" % (tmp_chr_id, ','.join(tmp_left_gene_names),
                                  tmp_q_gene, ','.join(tmp_right_gene_names)))
