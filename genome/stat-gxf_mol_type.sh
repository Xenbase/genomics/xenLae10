#!/bin/bash
GFF=$1

grep -v ^# $GFF | awk -F"\t" '{print $3}' | sort | uniq -c | sort -n
