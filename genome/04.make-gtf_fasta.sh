#!/bin/bash

GENOME="$HOME/pub/xenbase/xenLae10.fa"

GTF=$1

# for GTF in $(ls *gtf)
# do

  TX_FA=${GTF/.gtf/}".gtf_tx.fa"
  TX_LOG=${GTF/.gtf/}".gtf_tx.log"
  echo "Make $TX_FA"
  gffread -v -w $TX_FA -g $GENOME $GTF &>$TX_LOG

  CDS_FA=${GTF/.gtf/}".gtf_CDS.fa"
  CDS_LOG=${GTF/.gtf/}".gtf_CDS.log"
  gffread -v -x $CDS_FA -g $GENOME $GTF &>$CDS_LOG

  PROT_FA=${GTF/.gtf/}".gtf_prot.fa"
  PROT_LOG=${GTF/.gtf/}".gtf_prot.log"
  gffread -v -y $PROT_FA -g $GENOME $GTF &>$PROT_LOG

# done
