# Genomes of *Xenopus laevis*

Genomes from different sources have different format of sequence IDs. The files linked here are all followed the UCSC-style chromosome ID nomenclatures.
* UCSC: chr1L, chr1S, ..., chrM, chrUn_NC_001573_1, ...
* NCBI: NC_054371.1, NC_054372.1, ..., NC_001573.1, NC_001573.1, ...
* EnsEMBL: 1L, 1S, ..., MT, NW_024467173.1, ...
* Xenbase: Chr1L, Chr1S, ..., MT, Sca1, Sca2, ...

## Current version: xenLae10 (also known as v10.1)
Incorporating PacBio long-read sequencing data to fill the gaps, and correct the misassembly [Jessen2021].

### Public links
* NCBI: GCF_017654675.1 (refseq); GCA_017654675.1 (genbank)	
  * Genome report: https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_017654675.1/
  * FTP: https://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_other/Xenopus_laevis/all_assembly_versions/GCF_017654675.1_Xenopus_laevis_v10.1/

* UCSC: GCF_017654675.1
  * Genome browser: https://genome.ucsc.edu/cgi-bin/hgTracks?db=hub_2243239_GCF_017654675.1
  * FTP: https://hgdownload.soe.ucsc.edu/hubs/GCF/017/654/675/GCF_017654675.1/


## Previous version: xenLae2 (also known as v9.2)
First version of chromosome-scale genome. It was slightly modified from the original genome analyzed the International Xenopus Genome Project during NCBI/Genbank submission. 

### Public links
* NCBI: GCF_001663975.1 (refseq); GCA_001663975.1 (genbank)
  * Genome report: https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_001663975.1/
  * FTP: https://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_other/Xenopus_laevis/all_assembly_versions/suppressed/GCF_001663975.1_Xenopus_laevis_v2/

* UCSC: xenLae2
  * Genome browser: https://genome.ucsc.edu/cgi-bin/hgTracks?db=xenLae2
  * FTP: https://hgdownload.soe.ucsc.edu/goldenPath/xenLae2/bigZips/

## Historical archives
* v9.1 - This is the original version of the Xenopus laevis genome project consortium, submitted to the Genbank. 

* v8 - Intermediate version. Not used in major analysis/annotation. 

* v7
