#!/bin/bash
GFF=$1

grep -v ^# $GFF | awk -F"\t" '{print $2}' | sort | uniq -c | sort -n
