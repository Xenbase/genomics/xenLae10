#!/usr/bin/env python3
import sys

filename_gtf = sys.argv[1]

f_gtf = open(filename_gtf, 'r')
f_log = open('%s.concise.log' % filename_gtf, 'w')
for line in f_gtf:
    if line.startswith('#'):
        print(line.strip())
        continue
    
    tokens = line.strip().split("\t")
    str_coord = "\t".join(tokens[:8])
    tmp_source = tokens[1]
    tmp_mol_type = tokens[2]

    # 3 Curated Genomic
    # 126 RefSeq
    # 4460 cmsearch
    # 4847 BestRefSeq%2CGnomon
    # 8070 tRNAscan-SE
    # 230916 BestRefSeq
    # 2034290 Gnomon

    if tmp_source not in ['RefSeq', 'BestRefSeq', 'Gnomon', 'BestRefSeq%2CGnomon']:
        continue

    attr_list = dict()
    for tmp_attr in tokens[8].split(';'):
        tmp_attr = tmp_attr.strip()
        if tmp_attr.startswith('db_xref "GeneID:'):
            attr_list['gene_id'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('gene '):
            attr_list['gene_name'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('transcript_id '):
            attr_list['transcript_id'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('protein_id '):
            attr_list['protein_id'] = tmp_attr.split()[1].replace('"', '')
        if tmp_attr.startswith('exon_number '):
            attr_list['exon_number'] = tmp_attr.split()[1].replace('"', '')
    
    if 'gene_id' in attr_list \
        and attr_list['gene_id'].find('unassigned') >= 0:
        sys.stderr.write('Skip %s\n' % line.strip())
        f_log.write('Skip\t%s\n' % line.strip())
        continue
    
    if 'transcript_id' in attr_list \
        and attr_list['transcript_id'].find('unassigned') >= 0:
        sys.stderr.write('Skip %s\n' % line.strip())
        f_log.write('Skip\t%s\n' % line.strip())
        continue

    tmp_new_attr = ''
    
    if tmp_mol_type == 'gene':
        tmp_new_attr = 'gene_id "%s";' % attr_list['gene_id']

    if tmp_mol_type == 'transcript':
        tmp_new_attr = 'transcript_id "%s"; gene_id "%s";' %\
                       (attr_list['transcript_id'], 
                        attr_list['gene_id'])

    if tmp_mol_type == 'exon':
        tmp_exon_id = '%s-exon%s' % (attr_list['transcript_id'],
                                     attr_list['exon_number'])
        tmp_new_attr = 'exon_id "%s"; exon_number "%s";' %\
                       (tmp_exon_id, attr_list['exon_number'])
        tmp_new_attr += ' transcript_id "%s"; gene_id "%s";' %\
                        (attr_list['transcript_id'],
                         attr_list['gene_id'])

    if tmp_mol_type == 'CDS':
        tmp_new_attr = 'protein_id "%s"; exon_number "%s";' %\
                       (attr_list['protein_id'], attr_list['exon_number'])
        tmp_new_attr += ' transcript_id "%s"; gene_id "%s";' %\
                        (attr_list['transcript_id'],
                         attr_list['gene_id'])

    if tmp_new_attr != '' and 'gene_name' in attr_list:
        tmp_new_attr = '%s gene_name "%s";' % (tmp_new_attr,
                                               attr_list['gene_name'])

    if tmp_new_attr != '':
        print("%s\t%s" % (str_coord, tmp_new_attr))

f_gtf.close()
f_log.close()
