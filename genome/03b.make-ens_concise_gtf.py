#!/usr/bin/env python3
import sys

filename_gtf = sys.argv[1]

f_gtf = open(filename_gtf, 'r')

if filename_gtf.endswith('.gz'):
    import gzip
    f_gtf = gzip.open(filename_gtf, 'rt')

for line in f_gtf:
    if line.startswith('#'):
        print(line.strip())
        continue

    tokens = line.strip().split("\t")
    str_coord = "\t".join(tokens[:8])

    attr_list = dict()
    for tmp_attr in tokens[8].split(';'):
        for tmp_key in ['gene_id', 'gene_version', 'gene_name', 'gene_biotype',
                        'transcript_id', 'transcript_version',
                        'transcript_biotype',
                        'exon_id', 'exon_version', 'exon_number',
                        'protein_id', 'protein_version']:

            tmp_attr = tmp_attr.strip()
            if tmp_attr.startswith(tmp_key):
                tmp_attr = tmp_attr.replace('%s ' % tmp_key, '')
                attr_list[tmp_key] = tmp_attr.replace('"', '')

    tmp_mol_type = tokens[2]
    tmp_new_attr = ''

    if tmp_mol_type == 'gene':
        tmp_gene_id = '%s.%s' % (attr_list['gene_id'],
                                 attr_list['gene_version'])
        tmp_new_attr = 'gene_id "%s";' % tmp_gene_id

    if tmp_mol_type == 'transcript':
        tmp_gene_id = '%s.%s' % (attr_list['gene_id'],
                                 attr_list['gene_version'])
        tmp_tx_id = "%s.%s" % (attr_list['transcript_id'],
                               attr_list['transcript_version'])
        tmp_new_attr = 'transcript_id "%s"; gene_id "%s";' %\
                       (tmp_tx_id, tmp_gene_id)

    if tmp_mol_type == 'exon':
        tmp_gene_id = '%s.%s' % (attr_list['gene_id'],
                                 attr_list['gene_version'])
        tmp_tx_id = "%s.%s" % (attr_list['transcript_id'],
                               attr_list['transcript_version'])
        tmp_exon_id = '%s.%s' % (attr_list['exon_id'],
                                 attr_list['exon_version'])
        tmp_new_attr = 'exon_id "%s"; exon_number "%s";' %\
                       (tmp_exon_id, attr_list['exon_number'])
        tmp_new_attr += ' transcript_id "%s"; gene_id "%s";' %\
                        (tmp_tx_id, tmp_gene_id)

    if tmp_mol_type == 'CDS':
        tmp_gene_id = '%s.%s' % (attr_list['gene_id'],
                                 attr_list['gene_version'])
        tmp_tx_id = "%s.%s" % (attr_list['transcript_id'],
                               attr_list['transcript_version'])
        tmp_prot_id = '%s.%s' % (attr_list['protein_id'],
                                 attr_list['protein_version'])
        tmp_new_attr = 'protein_id "%s"; exon_number "%s";' %\
                       (tmp_prot_id, attr_list['exon_number'])
        tmp_new_attr += ' transcript_id "%s"; gene_id "%s";' %\
                        (tmp_tx_id, tmp_gene_id)

    if tmp_new_attr != '' and 'gene_name' in attr_list:
        tmp_new_attr = '%s gene_name "%s";' % (tmp_new_attr,
                                               attr_list['gene_name'])

    if tmp_new_attr != '':
        print("%s\t%s" % (str_coord, tmp_new_attr))

f_gtf.close()
