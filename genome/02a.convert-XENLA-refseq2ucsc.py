#!/usr/bin/env python3
import sys
import os

filename_in = sys.argv[1]

filename_alias = 'raw/GCF_017654675.1_Xenopus_laevis_v10.1_assembly_report.txt'
dirname_base = os.path.dirname(os.path.realpath(__file__))

chr_alias = dict()
f_alias = open(os.path.join(dirname_base, filename_alias), 'r')
# Sequence-Name Sequence-Role   Assigned-Molecule       Assigned-Molecule-Location/Type GenBank-Accn    Relationship    RefSeq-Accn     Assembly-Unit   Sequence-Length UCSC-style-name
# Chr1L   assembled-molecule      1L      Chromosome      CM030340.1      =       NC_054371.1     Primary Assembly        233740091       na
# Sca1    unplaced-scaffold   na  na  JAGEVR010000036.1   =   NW_024467162.1  Primary Assembly    62059   na
# MT  assembled-molecule  MT  Mitochondrion   na  <>  NC_001573.1 non-nuclear 17553   na

for line in f_alias:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    refseq_id = tokens[6]
    chr_id = tokens[0].replace('Chr', 'chr')
    if tokens[0].startswith('Sca'):
        chr_id = 'chrUn_%s' % tokens[6].replace('.', 'v')
    elif tokens[0].startswith('MT'):
        chr_id = 'chrM'
    chr_alias[refseq_id] = chr_id
f_alias.close()


f_in = open(filename_in, 'r')
if filename_in.endswith('.gz'):
    import gzip
    f_in = gzip.open(filename_in, 'rt')

f_out = open('%s.ucsc_style' % filename_in, 'w')
for line in f_in:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
    else:
        tokens = line.strip().split("\t")
        tokens[0] = chr_alias[tokens[0]]
        f_out.write("%s\n" % "\t".join(tokens))
f_in.close()
