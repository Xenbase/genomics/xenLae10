#!/usr/bin/env python3
import sys
import os

filename_in = sys.argv[1]

filename_alias = 'raw/GCF_017654675.1_Xenopus_laevis_v10.1_assembly_report.txt'
dirname_base = os.path.dirname(os.path.realpath(__file__))

chr_alias = dict()
f_alias = open(os.path.join(dirname_base, filename_alias), 'r')
# Sequence-Name	Sequence-Role	Assigned-Molecule	Assigned-Molecule-Location/Type	GenBank-Accn	Relationship	RefSeq-Accn	Assembly-Unit	Sequence-Length	UCSC-style-name
# Chr1L	assembled-molecule	1L	Chromosome	CM030340.1	=	NC_054371.1	Primary Assembly	233740091	na
# Chr1S	assembled-molecule	1S	Chromosome	CM030341.1	=	NC_054372.1	Primary Assembly	202412971	na
# MT	assembled-molecule	MT	Mitochondrion	na	<>	NC_001573.1	non-nuclear	17553	na

for line in f_alias:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    ens_id = tokens[2]
    ens_acc_id = tokens[4]

    chr_id = tokens[0].replace('Chr', 'chr')
    if tokens[0].startswith('Sca'):
        chr_id = 'chrUn_%s' % tokens[6].replace('.', 'v')
    elif tokens[0].startswith('MT'):
        chr_id = 'chrM'

    chr_alias[ens_id] = chr_id
    chr_alias[ens_acc_id] = chr_id
f_alias.close()

# 1L      ensembl gene    233686061       233706086       .       +       .       gene_id "ENSXLAG00005000039"; gene_version "1"; gene_source "ensembl"; gene_biotype "protein_coding";
# JAGEVR010000001.1       ensembl exon    2       430     .       +       .       gene_id "ENSXLAG00005000122"; gene_version "1"; transcript_id "ENSXLAT00005000286"; transcript_version "1"; exon_number "1"; gene_source "ensembl"; gene_biotype "protein_coding"; transcript_source "ensembl"; transcript_biotype "protein_coding"; exon_id "ENSXLAE00005001169"; exon_version "1"; projection_parent_transcript "ENST00000298283.5";

f_in = open(filename_in, 'r')
if filename_in.endswith('.gz'):
    import gzip
    f_in = gzip.open(filename_in, 'rt')

f_out = open('%s.ucsc_style' % filename_in, 'w')
for line in f_in:
    if line.startswith('#'):
        f_out.write("%s\n" % line.strip())
    else:
        tokens = line.strip().split("\t")
        tokens[0] = chr_alias[tokens[0]]
        f_out.write("%s\n" % "\t".join(tokens))
f_in.close()
